(function ($) {

    function isMobile() {
        return $(window).width() < 992;
    }

    $(function () {
        if (sessionStorage['intro-done'] !== "yes" && !isMobile()) {
            sessionStorage['intro-done'] = "yes";
            setTimeout(function () {
                $(".fullscreen-bg").fadeOut(500);
            }, 9000);
        } else {
            $(".fullscreen-bg").addClass("hide");
        }

        $("nav ul li a").click(function (evt) {
            evt.preventDefault();
            var href = $(this).attr("href");
            if (!(href.indexOf("#") > -1)) {
                $(document.body).fadeOut(200);
                window.setTimeout(function () {
                    window.location.href = href;
                }, 250);
            }
        });

        if (isMobile()) {
            if ($(".slider").length > 0) {
                var h = $(window).height();
                $(window).on("scroll", function (evt) {
                    var scr = $(window).scrollTop();
                    $('.slider').each(function (index, elem) {
                        var y = $(elem).offset().top;
                        var eH = $(elem).height();
                        if (y - scr < h / 2 && y + eH - scr > h / 2) {
                            if (!$(elem).hasClass('show-content')) {
                                $(elem).addClass('show-content');
                            }
                        } else {
                            if ($(elem).hasClass('show-content')) {
                                $(elem).removeClass('show-content');
                            }
                        }
                    });
                });
            }
        }

    });
})(jQuery);