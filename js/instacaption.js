$(".insta-caption").each(function() {
  var atR = /(@\w+)/g;
  var hashR = /(#\w+)/g;
  var txt = $(this).html();
  var atM = [];
  var hashM = [];
  if(atR.test(txt)) {
    txt = txt.replace(atR, "<span class='at-name'>$1</span>");
    $(this).html(txt);
  }
  if(hashR.test(txt)) {
    txt = txt.replace(hashR, "<span class='hashtag'>$1</span>");
    $(this).html(txt);
  }
});

