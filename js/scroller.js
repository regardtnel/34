(function () {
    $('.owl-carousel').owlCarousel({
        lazyLoad: true,
        loop: true,
        margin: 10,
        autoplay:true,
        autoplayTimeout:100,
        autoplayHoverPause:true,
        responsive: {
            0: {
                items: 2
            },
            480: {
                items: 4
            },
            768: {
                items: 9
            }
        }
    });
    
})();

